<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="#">
      <img src="img/logo.png" class="logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="#about">About </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#cari">Cari</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Kategori
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#produk">Komputer</a>
            <a class="dropdown-item" href="#produk">Leptop</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#produk">Monitor</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#produk">Produk</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="CARI" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">CARI</button>
      </form>
    </div>
  </nav>