@extends('template.content1')

@section('content1')
 <!-- Carousel -->
 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="img/bg1.jpeg" alt="First slide">
        <div class="carousel-caption d-none d-md-block">
          <h5>Labolatorium E-Commerce</h5>
          <p>Menjual Segala Keperluan Komputer Anda Dari yang <b>TERLENGKAP</b> Terkecil Sampai yang Terbesar. Pengiriman Barang <b>TERCEPAT</b> yang anada inginkan. Telah <b>TERCEPAT</b> selama puluhan tahun</p>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/bg2.jpg" alt="Second slide">
        <div class="carousel-caption d-none d-md-block">
          <h5>Labolatorium E-Commerce</h5>
          <p>Menjual Segala Keperluan Komputer Anda Dari yang <b>TERLENGKAP</b> Terkecil Sampai yang Terbesar. Pengiriman Barang <b>TERCEPAT</b> yang anada inginkan. Telah <b>TERCEPAT</b> selama puluhan tahun</p>
        </div>
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="img/bg3.jpg" alt="Third slide">
        <div class="carousel-caption d-none d-md-block">
          <h5>Labolatorium E-Commerce</h5>
          <p>Menjual Segala Keperluan Komputer Anda Dari yang <b>TERLENGKAP</b> Terkecil Sampai yang Terbesar. Pengiriman Barang <b>TERCEPAT</b> yang anada inginkan. Telah <b>TERCEPAT</b> selama puluhan tahun</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <!-- About Us -->
  <section class="about" id="about">
    <div class="container">
      <div class="row">
        <div class="col text-center">
          <h2>ABOUT US</h2>
        </div>
      </div>
      <div class="row">
        <div class="col text-justify">
          <h5>Labolatororium E-commerce</h5>
          <p>Menjual Segala Keperluan Komputer Anda Dari yang <b>TERLENGKAP</b> Terkecil Sampai yang Terbesar. Pengiriman Barang <b>TERCEPAT</b> yang anada inginkan. Telah <b>TERCEPAT</b> selama puluhan tahun</p>
        </div>
        <div class="col">
          TERLENGKAP
          <div class="progress" style="height: 10px;">
            <div class="progress-bar progress-bar-striped bg-dark" role="progressbar" style="width: 75%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          TERCEPAT
          <div class="progress" style="height: 10px;">
            <div class="progress-bar progress-bar-striped bg-dark" role="progressbar" style="width: 80%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
          TERPERCAYA
          <div class="progress" style="height: 10px;">
            <div class="progress-bar progress-bar-striped bg-dark" role="progressbar" style="width: 85%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- JUMBOTRON -->
  <div class="jumbotron jumbotron-fluid" id="cari">
    <div class="container">
      <form class="cari">
        <div class="form-row">
          <div class="col-md-10">
            <input type="text" class="form-control" placeholder="CARI BARANG">
          </div>
          <button type="submit" class="btn btn-outline-dark">CARI</button> 
        </div>
      </form>
    </div> 
  </div>
<!-- AKHIR JUMBOTRON -->

<!-- PRODUK -->
<section class="produk" id="produk">
  <div class="container">
    <div class="row">
      <div class="col text-center">
        <h2>PRODUK TERBARU</h2>
        <br>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">Harddisk</div>
          <img class="card-img-top" src="img/1.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">HARDDISK LACIE</h5>
            <h6>Rp.500.000</h6>
            <p class="card-text">Stok : 20</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">Leptop</div>
          <img class="card-img-top" src="img/2.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">HP PAVILLION X360</h5>
            <h6>Rp.12.000.000</h6>
            <p class="card-text">Stok : 10</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">Processor</div>
          <img class="card-img-top" src="img/3.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">INTEL CORE i9</h5>
            <h6>Rp.8.000.000</h6>
            <p class="card-text">Stok : 5</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">Keyboard</div>
          <img class="card-img-top" src="img/4.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">KEYBOARD RAZER</h5>
            <h6>Rp.880.000</h6>
            <p class="card-text">Stok : 2</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">Headset</div>
          <img class="card-img-top" src="img/5.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">HEADSET RAZER</h5>
            <h6>Rp.300.000</h6>
            <p class="card-text">Stok : 2</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">CPU</div>
          <img class="card-img-top" src="img/6.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">CPU GAMING</h5>
            <h6>Rp.10.000.000</h6>
            <p class="card-text">Stok : 10</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">RAM</div>
          <img class="card-img-top" src="img/7.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">RAM CORSAIR 8GB</h5>
            <h6>Rp.1.200.000</h6>
            <p class="card-text">Stok : 18</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
          <div class="card-header">MONITOR</div>
          <img class="card-img-top" src="img/8.png" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">MONITOR LED HP</h5>
            <h6>Rp.2.000.000</h6>
            <p class="card-text">Stok : 8</p>
            <a href="#" class="btn btn-info">detail</a>
            <a href="#" class="btn btn-success">beli</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- AKHIR PRODUK -->


@endsection()